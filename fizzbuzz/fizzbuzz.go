package fizzbuzz

import (
	"net/http"
	"strconv"

	"github.com/labstack/echo"
)

type (
	user struct {
		ID   int    `json:"id"`
		Name string `json:"name"`
	}
)

var (
	users = map[int]*user{}
	seq   = 1
)

func Fizzbuzz(c echo.Context) error {
	id, _ := strconv.Atoi(c.Param("id"))
	x := id

	u := &user{
		ID:   x,
		Name: "",
	}

	if err := c.Bind(u); err != nil {
		return err
	}
	users[u.ID] = u
	switch {
	case x%15 == 0:
		users[u.ID].Name = "fizzbuzz"
		return c.JSON(http.StatusOK, users[id])
	case x%5 == 0:
		users[u.ID].Name = "buzz"
		return c.JSON(http.StatusOK, users[id])
	case x%3 == 0:
		users[u.ID].Name = "fizz"
		return c.JSON(http.StatusOK, users[id])
	default:
		users[u.ID].Name = strconv.Itoa(x)
		return c.JSON(http.StatusOK, users[id])
	}
}
