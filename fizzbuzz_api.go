package main

import (
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"

	fz "gitlab.com/healtyboy/gtdd/fizzbuzz"
)

func main() {
	e := echo.New()

	// Middleware
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	// Routes
	e.GET("/users/:id", fz.Fizzbuzz)

	// Start server
	e.Logger.Fatal(e.Start(":1323"))
}
